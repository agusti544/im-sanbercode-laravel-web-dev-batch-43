@extends('layouts.master')

@section('title')
    Halaman Biodata
@endsection
@section('sub-title')
    Biodata
@endsection
@section('content')
<form action="/welcome" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="FN"><br><br>
        <label>Last Name</label><br>
        <input type="text" name="LN"><br><br>
        <label>Gender</label><br>
        <input type="radio" name="kelamin">Male<br>
        <input type="radio" name="kelamin">Female<br>
        <input type="radio" name="kelamin">Other<br><br>
        <label>Nationality</label><br>
        <select name="national">
            <option value="Indonesia">Indonesia</option>
            <option value="Japan">Japan</option>
            <option value="Belanda">Belanda</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="bahasa">Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br><br>
        <label>Bio</label><br>
        <textarea name="message" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="submit">
    </form>
@endsection
    