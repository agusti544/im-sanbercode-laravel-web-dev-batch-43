@extends('layouts.master')

@section('title')
    Halaman Welcome
@endsection

@section('sub-title')
    Welcome
@endsection

@section('content')
    <h1>Selamat Datang, {{$namaDepan}} {{$namaBelakang}}</h1>
    <h2>Terimakasih Telah Bergabung di SanberBook. Social Media Kita Bersama!</h2>
@endsection

