@extends('layouts.master')

@section('title')
    Halaman Detai Cast
@endsection

@section('sub-title')
    Detail Cast
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<h4>{{$cast->umur}} Tahun</h4>
<p>{{$cast->bio}}</p>
@endsection