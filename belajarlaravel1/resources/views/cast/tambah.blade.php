@extends('layouts.master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('sub-title')
    Halaman Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Cast Nama</label>
    <input type="text" name="castName" class="form-control">
  </div>
  @error('castName')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Cast Umur</label>
    <input type="text" name="castUmur" class="form-control">
  </div>
  @error('castUmur')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Cast Bio</label>
    <textarea name="castBio" class="form-control" cols="30" row="10"></textarea>
  </div>
  @error('castBio')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection