@extends('layouts.master')

@section('title')
    Halaman Edit Cast
@endsection

@section('sub-title')
    Halaman Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Cast Nama</label>
    <input type="text" name="castName" value="{{$cast->nama}}" class="form-control">
  </div>
  @error('castName')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Cast Umur</label>
    <input type="text" name="castUmur" value="{{$cast->umur}}" class="form-control">
  </div>
  @error('castUmur')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Cast Bio</label>
    <textarea name="castBio" class="form-control" cols="30" row="10">{{$cast->bio}}</textarea>
  </div>
  @error('castBio')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection