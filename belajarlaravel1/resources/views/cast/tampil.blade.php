@extends('layouts.master')

@section('title')
    Halaman Tampil Cast
@endsection

@section('sub-title')
    Halaman Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm">Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($casts as $key => $item)
        <tr>
            <th scope="row">{{$key +1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
              <form action="/cast/{{$item->id}}" method="POST">
                <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
              </from>
            </td>
        </tr>
    @empty
        <h1>Data Kosong</h1>
    @endforelse
  </tbody>
</table>
@endsection