@extends('layouts.master')

@section('title')
    Halaman Tampil Genre
@endsection

@section('sub-title')
    Halaman Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary btn-sm">Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genres as $key => $item)
        <tr>
            <th scope="row">{{$key +1}}</th>
            <td>{{$item->nama}}</td>
            <td>
                <a href="/genre/create" class="btn btn-primary btn-sm">Detail</a>
            </td>
        </tr>
    @empty
        <h1>Data Kosong</h1>
    @endforelse
  </tbody>
</table>
@endsection