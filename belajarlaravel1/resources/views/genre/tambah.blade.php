@extends('layouts.master')

@section('title')
    Halaman Tambah Genre
@endsection

@section('sub-title')
    Halaman Genre
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
  <div class="form-group">
    <label>Genre Name</label>
    <input type="text" name="genreName" class="form-control">
  </div>
  @error('genreName')
      <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection