<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create() {
        return view('cast.tambah');
    }

    public function store(Request $request) {
        //error validasi
        $request->validate([
            'castName' => 'required',
            'castUmur' => 'required',
            'castBio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['castName'],
            'umur' => $request['castUmur'],
            'bio' => $request['castBio']
        ]);
        return redirect('/cast');
    }

    public function index(){
        $casts = DB::table('cast')->get();
        return view('cast.tampil', ['casts' => $casts]);
    }

    public function show($id) {
        $cast = DB::table('cast')->find($id);
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id) {
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id) {
        //error validasi
        $request->validate([
         'castName' => 'required',
         'castUmur' => 'required',
         'castBio' => 'required',
     ]);

     DB::table('cast')
         ->where('id',$id)
         ->update ([
             'nama' => $request['castName'],
             'umur' => $request['castUmur'],
             'bio' => $request['castBio']
         ]);
         return redirect('/cast');
    }
    
    public function destory($id) {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
