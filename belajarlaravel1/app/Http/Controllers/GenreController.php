<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function create() {
        return view('genre.tambah');
    }

    public function store(Request $request) {
        //error validasi
        $request->validate([
            'genreName' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' => $request['genreName']
        ]);
        return redirect('/genre');
    }

    public function index(){
        $genres = DB::table('genre')->get();
        return view('genre.tampil', ['genres' => $genres]);
    }

    public function show($id) {
        $genre = DB::table('gebre')->find($id);
        return view('genre.detail', ['genre' => $genre]);
    }

    
}
