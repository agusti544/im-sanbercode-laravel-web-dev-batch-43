<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function Form() {
        return view('page.form');
    }

    public function send(Request $request) {
        //dd($request);
        $namaDepan = $request['FN'];
        $namaBelakang = $request['LN'];
        return view('page.home',['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
