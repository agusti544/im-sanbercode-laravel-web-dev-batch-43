<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\castController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::Class, 'Beranda']);
Route::get('/form', [FormController::Class, 'Form'])->name('form');

Route::post('/welcome', [FormController::class, 'send']); 


Route::get('/data-table', function() {
    return view('page.data-table');
});

Route::get('/table', function() {
    return view('page.table');
});

// CRUD Category
//create data
//rule untuk mengarahkan ke halaman form input
Route::get('/genre/create/', [GenreController::class, 'create']);
Route::get('/cast/create/', [castController::class, 'create']);

//rule untuk memasukan imputan kedalam database
Route::post('genre', [GenreController::class,'store']);
Route::post('cast', [castController::class,'store']);
//read data
//rule untuk tampil semua data yang ada di db
Route::get('/genre', [GenreController::class, 'index']);
Route::get('/cast', [castController::class, 'index']);
Route::get('/cast/{id}', [castController::class, 'show']);

//update data
Route::get('/cast/{id}/edit', [castController::class, 'edit']);
Route::put('/cast/{id}', [castController::class, 'update']);

//delete data
Route::delete('/cast/{id}', [castController::class, 'destory']);